# Performance Optimierung für Virtual Reality in Unity
---
- Warum sollte man optimieren?
- Performance Tipps
- Tools zur Performance Analyse

---
### Why?

![Vomitting Pumpkin](/Images/Vomit_Pumpkin.jpg)
+++
### Why?
- Mindestens 90 fps
- VR benötigt mehr Leistung
![2 Eyes Rendering](/Images/3dmark-virtual-reality-benchmark.jpg)
---

### Performance Tipps
+++

#### Polycount
![low Poly Art](/Images/lowPoly_art.jpg)
+++
#### Lighting
![SkyrimLighting](/Images/SkyrimLighting.jpg)
+++
#### LOD
![LOD](/Images/4_lod0.png)
![LOD](/Images/5_lod1.png)
+++
#### Frustum Culling
![No Culling](/Images/6_occlusionfrustumculling.png)
+++
#### Occlusion Culling
![Culling](/Images/7_occlusionfullculling.png)
+++
#### Draw Call Batching
- Dynamic Batching:
- - Automatisch
- - Für Objekte mit dem selben Material
- Static Batching
- - Alle Objekte, die im Inspector als "Static" markiert sind und das gleiche Material haben

+++
#### Instancing
![GPU Instancing Material 1](/Images/UnityGPUInstancingMaterial.PNG)
![GPU Instancing Material 2](/Images/UnityGPUInstancingMaterial2.PNG)
+++
#### Instancing
![GPU Instancing Code](/Images/UnityGraphicsDrawMeshInstanced.PNG)
+++

#### Single Pass Stereo Rendering
- Für PC und PS4
- Ein Renderpass für beide Augen

+++
#### Single Pass Stereo Rendering
![Render Normal](/Images/SPS_001.PNG)
+++
#### Single Pass Stereo Rendering
![Single Pass Render](/Images/SPS_002.PNG)
+++
#### Single Pass Stereo Rendering
![Unity Checkbox](/Images/SinglePassStereoRendering3.png)
+++
#### Shader
- Post Processing Effekte ziehen Performance in VR
- PBR Shader (Unity Standard Shader) ziehen viel Performance
- Evtl. Mobile Shader benutzen
- Möglichst geringe Anzahl unterschiedlicher Materials
- Transparenz ist relativ performancelastig

+++

#### Partikeleffekte
- Viele Partikel sorgen schnell für schlechte Performance in VR
- Verbesserung mit Instancing

+++
#### Code
- GetComponent<>() und GameObject.Find() sind sehr performancelastig 
    und sollten nie im Update stattfinden

---
### Tools zur Performance Analyse

+++
#### Stats Window
![Stats](/Images/UnityStats.PNG)

+++
#### Unity Profiler
![Profiler](/Images/UnityProfiler.PNG)

+++
#### Unity Frame Debugger
![Frame Debugger](/Images/UnityFrameDebugger.PNG)

+++
#### RenderDoc
![RenderDoc](/Images/RenderDoc.PNG)

---

### Noch Fragen?